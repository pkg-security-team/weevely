weevely (4.0.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 4.0.2.
  * debian/control: update Debian Packaging Policy to 4.7.0 -
    no changes needed.
  * debian/copyright: remove EasyTar's from lcopyright stanza -
    file removed by upstream.
  * debian/watch: update filenamemangle pattern.

 -- Antonio Ladeia <contato@antonioladeia.com>  Sat, 18 May 2024 06:12:54 -0300

weevely (4.0.1-2) unstable; urgency=medium

  [ Samuel Henrique ]
  * Bump Standards-Version to 4.6.2
  * Configure git-buildpackage for Debian
  * d/lintian-overrides: Update finding names
  * d/watch: Fix watch by using GitHub's API

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Refer to common license file for Apache-2.0.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.6.1, no changes needed.

 -- Samuel Henrique <samueloph@debian.org>  Sun, 15 Jan 2023 20:34:26 +0000

weevely (4.0.1-1) unstable; urgency=medium

  * New upstream version 4.0.1

 -- Samuel Henrique <samueloph@debian.org>  Mon, 06 Jan 2020 19:06:34 +0000

weevely (4.0.0-1) unstable; urgency=medium

  * New upstream version 4.0.0
  * Bump DH to v12
  * Bump Standards-Version to 4.4.1
  * Bump dependencies to python3 (closes: #945723)
  * Add salsa-ci.yml
  * d/copyright: Use https
  * d/control: Add Rules-Requires-Root: no

 -- Samuel Henrique <samueloph@debian.org>  Mon, 30 Dec 2019 02:07:06 +0000

weevely (3.7.0-1) unstable; urgency=medium

  * New upstream version 3.7.0

 -- Samuel Henrique <samueloph@debian.org>  Sat, 20 Oct 2018 14:00:31 -0300

weevely (3.6.2-2) unstable; urgency=medium

  * Bump Standards-Version to 4.2.1
  * d/control: Change Section to web (closes: #905900)
  * d/watch: Remove trailing whitespace

 -- Samuel Henrique <samueloph@debian.org>  Tue, 09 Oct 2018 20:44:05 -0300

weevely (3.6.2-1) unstable; urgency=medium

  * Initial Debian release (closes: #901983)
  * New upstream version 3.6.2
  * Bump DH level to 11
  * Bump Standards-Version to 4.1.4.1
  * Bump Watch to v4
  * Update Vcs-* field to point to salsa
  * symlink on /usr/bin instead of "cd + run" helper script
  * add lintian-overrides, commented file with reasons
  * d/control
    - Priority: from extra to optional
    - from kali to debian under DPMT team
  * d/copyright:
    - add entry for user-agents
    - update debian/* entry
  * d/dirs: no need to use
  * d/rules: remove comments
  * d/weevely.manpages: add manpage
  * wrap-and-sort -a

 -- Samuel Henrique <samueloph@debian.org>  Thu, 28 Jun 2018 01:52:28 -0300

weevely (3.2.0-0kali3) kali-dev; urgency=medium

  * Replace depends on python-pysocks by python-socks

 -- Sophie Brun <sophie@freexian.com>  Tue, 12 Jan 2016 11:38:42 +0100

weevely (3.2.0-0kali2) kali-dev; urgency=medium

  * Drop patch (fix-sys-dir) and replace it by a helper-script

 -- Sophie Brun <sophie@freexian.com>  Fri, 24 Jul 2015 09:16:16 +0200

weevely (3.2.0-0kali1) kali-dev; urgency=medium

  * Import new upstream release
  * Refresh patch
  * Add depends on python-prettytable, python-mako, python-pysocks,
    python-dateutil
  * Change Homepage: https://github.com/epinna/weevely3 (instead of
    https://github.com/epinna/Weevely/)

 -- Sophie Brun <sophie@freexian.com>  Thu, 23 Jul 2015 16:25:42 +0200

weevely (1.1-1kali0) kali; urgency=low

  * Update from git

 -- Emanuele Acri <crossbower@kali.org>  Thu, 23 Jan 2014 16:02:32 +0100

weevely (1.0-1kali2) kali; urgency=low

  * Updated watch file

 -- Mati Aharoni <muts@kali.org>  Sun, 12 Jan 2014 17:41:12 -0500

weevely (1.0-1kali1) kali; urgency=low

  * Remove desktop file

 -- Mati Aharoni <muts@kali.org>  Sat, 15 Dec 2012 13:50:48 -0500

weevely (1.0-1kali0) kali; urgency=low

  * Initial release

 -- Devon Kearns <dookie@kali.org>  Tue, 11 Dec 2012 11:33:15 -0700
